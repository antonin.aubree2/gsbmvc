<h2>Détail de la fiche de frais</h2>
<div>
  <div>
    Fiche frais du mois 
    <?php
        $date = substr($ficheFrais['mois'], 4,2).'-'.substr($ficheFrais['mois'], 0,4);
        echo($date);
    ?>
  </div>
  <div>
  <strong>Visiteur : </strong><?php echo($visiteur[0]['nom']." ".$visiteur[0]['prenom']);?>
  <br/>
  <strong>Etat : </strong><span><?php echo($ficheFrais['libEtat']); ?></span>
  <br/>
  <strong>Montant validé : </strong> <?php echo($ficheFrais['montantValide']) ?> €
  </div>
  
</div>
<br/>

<table>
    <thead>
        <tr>
            <td>
                <strong>Elements forfaitisés</strong>
            </td>
        </tr>
        <tr>
            <td><strong>Frais</strong></td>
            <td><strong>Montant unitaire</strong></td>
            <td><strong>Quantité</strong></td>
            <td><strong>Montant total</strong></td>
        </tr>
    </thead>
    <tbody>
    <?php 
        $montantTotalF = 0;
        for($i = 0; $i < count($infoEtat); $i++) {
            $libelle = $infoEtat[$i]['libelle'];
            $montantUnitaire = $infoEtat[$i]['montant'];
            $quantite = $infoFicheFrais[$i]['quantite'];
            $montTotal = $quantite * $montantUnitaire;
            $montantTotalF += $montTotal;
            ?>
        <tr>
            <td><?php echo($libelle)?></td>
            <td><?php echo($montantUnitaire)?></td>
            <td><?php echo($quantite)?></td>
            <td><?php echo($montTotal)?> €</td>
        </tr>

        <?php } ?>
        
        
        <tr>
            <td><strong>Montant total</strong></td>
            
            <td><?php echo($montantTotalF)?> €</td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
        <tr>
            <td>
                <strong>Elements hors forfait - <?php echo($ficheFrais['nbJustificatifs']); ?> justificatifs reçus</strong>
            </td>
        </tr>
        <tr>
            <td><strong>Date</strong></td>
            <td><strong>Libellé</strong></td>
            <td><strong>Montant total</strong></td>
        </tr>
    </thead>
    <tbody>
    <?php 
        $montantTotalHF = 0;
        for($i = 0; $i < count($infoHorsForfait); $i++) {
            $libelle = $infoHorsForfait[$i]['libelle'];
            $montant = $infoHorsForfait[$i]['montant'];
            $date = $infoHorsForfait[$i]['date'];
            $montantTotalHF += $montant;
            ?>
        <tr>
            <td><?php echo($date)?></td>
            <td><?php echo($libelle)?></td>
            <td><?php echo($montant)?> €</td>
        </tr>

        <?php } ?>
        
        
        <tr>
            <td><strong>Montant total</strong></td>
            
            <td><?php echo($montantTotalHF)?> €</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <td>
                <strong>Total des frais</strong>
            </td>
        </tr>
        <tr>
            <td><strong>Total Frais Forfaitisés</strong></td>
            <td><strong>Total Frais Hors Forfait</strong></td>
            <td><strong>Total des frais à rembourser</strong></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php 
            $montantTotal = $montantTotalF + $montantTotalHF;
            ?>
            <td><?php echo($montantTotalF)?> €</td>
            <td><?php echo($montantTotalHF)?> €</td>
            <td><?php echo($montantTotal)?> €</td>
        </tr>
    </tbody>
</table>
