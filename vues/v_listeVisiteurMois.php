<div class="col">
    <h2>Validation des fiches frais</h2>
    <h3>Mois à sélectionner : </h3>
    <form action="index.php?uc=validerFrais&action=selection" method="post">
      <div class="corpsForm">
        <p>
          <label for="lstVisiteur" accesskey="n">Visiteur : </label>
          <select class="form-control" id="lstVisiteur" name="idVisiteur">
          <?php
            foreach ($visiteur as $v)
            {
              $nomVisiteur = $v['nom'];
              $prenomVisiteur =  $v['prenom'];

              if($v['id'] == $_REQUEST['idVisiteur']){
              ?>
                <option selected value="<?php echo $v['id'] ?>"><?php echo  $nomVisiteur." ".$prenomVisiteur ?> </option>
              <?php 
              }
              else{ ?>
                <option value="<?php echo $v['id'] ?>"><?php echo $nomVisiteur." ".$prenomVisiteur ?> </option>
              <?php 
              }
            } ?>    
          </select>
        </p>
        <?php if($visiteurSelected) { ?>
          <p>
            <label for="lstMois" accesskey="n">Mois : </label>
            <select class="form-control" id="lstMois" name="lstMois">
            <?php
              foreach ($moisDispo as $unMois)
              {
                $mois = $unMois['mois'];
                $numAnnee =  $unMois['numAnnee'];
                $numMois =  $unMois['numMois'];
                if($mois == $moisASelectionner){
                ?>
                  <option selected value="<?php echo $mois ?>"><?php echo  $numMois."/".$numAnnee ?> </option>
                <?php 
                }
                else{ ?>
                  <option value="<?php echo $mois ?>"><?php echo  $numMois."/".$numAnnee ?> </option>
                <?php 
                }
              } ?>    
            </select>
          </p>
        <?php } ?>
      </div>
  
      <div class="piedForm">
        <p>
          <input id="ok" type="submit" value="Valider" size="20" />
        </p> 
      </div>

    </form>
  </div>
