<?php


$build_query_http = http_build_query($_GET);;
$not_blank = array_filter($_GET, function($x) { return $x != ''; });

if ($not_blank != $_GET) {
    $query = http_build_query($not_blank);
    header('Location: ' . $_SERVER['PHP_SELF'] . "?$query") ;
    exit;
}
?>
<h2>Suivis du paiement des fiches de frais</h2>

<div>
  <div>
    <input type="submit" onclick="mettreEnPaiement()" value="Mettre en paiement"/>
    <input type="submit" onclick="confimerRemboursement()" value="Confirmer remboursement"/>
  </div>
</div>
<br/>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col"><input type="checkbox" onclick="checkEverything()"/></th>
        <th scope="col">Visiteur</th>
        <th scope="col">Mois</th>
        <th scope="col">Montant</th>
        <th scope="col">Date de modification</th>
        <th scope="col">Etat fiche</th>
        <th scope="col">Valider</th>
      </tr>
      <form action="index.php?uc=compt&action=suiviFichePaiement" method="get">
        <input type="hidden" name="uc" value="compt">
        <input type="hidden" name="action" value="suiviFichePaiement">
        <tr>
          <th scope="col"></th>
          <th scope="col">
            <select name="nomIdSelect" class="form-control">
              <option value="" selected disabled hidden></option>
              <?php
                foreach($vis as $v) {
                  $nom = $v['nom'];
                  $prenom = $v['prenom'];
                  $id = $v['id'];
                  echo('<option value="'.$id.'">'.$nom.' '.$prenom.'</option>');
                }
              ?>
            </select>
          </th>
          <th scope="col"><input class="form-control" type="text" name="mois" placeholder="AAAA-MM" pattern="^((2009)|(20[1-2][0-9]))-((0[1-9])|(1[0-2]))$" title="Enter a date in this format YYYY-MM-DD"/></th>
          <th scope="col"><input class="form-control" type="text" name="montant"/></th>
          <th scope="col"><input class="form-control" type="text" name="dateModif" placeholder="AAAA-MM-DD" pattern="^([0-9]{4}[-]?((0[13-9]|1[012])[-]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-]?31|02[-]?(0[1-9]|1[0-9]|2[0-8]))|([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00)[-]?02[-]?29)$" title="Enter a date in this format YYYY-MM-DD"/></th>
          <th scope="col">
            <select name="etatSelect" class="form-control">
              <option value="" selected disabled hidden></option>
              <?php
                foreach($typeEtat as $e) {
                  $libelle = $e['libelle'];
                  $id = $e['id'];
                  echo('<option value="'.$id.'">'.$libelle.'</option>');
                }
              ?>
            </select>
          </th>
          <th scope="col"><input type="submit" class="btn btn-primary" value="Chercher"/></th>
        </tr>
      </form>
    </thead>
    <tbody>
    <?php
      foreach($visiteurs as $v) {
          $idV = $v['idV'];
          $nom = $v['nom'];
          $prenom = $v['prenom'];
          $date = $v['mois'];
          $annee = substr($date, 0,4);
          $mois = substr($date,4,2);
          $dateModif = $v['dateModif'];
          $montantValide = $v['montantValide'];
          $etat = $v['etat'];?>

          <tr>
            <?php echo('<th scope="col"><input type="checkbox" class="elem-check" name="'.$idV.' '.$date.'"/></th>');?>

            <td><?php echo("$nom $prenom" ) ?></td>
            <td><?php echo("$annee-$mois") ?></td>
            <td><?php echo("$montantValide" ) ?></td>
            <td><?php echo("$dateModif" ) ?></td>
            <td><?php echo("$etat" ) ?></td>
            <td><?php echo('<a href="index.php?uc=compt&action=detailFichePaiement&idV='.$idV.'&mois='.$date.'" class="btn btn-success btn-sm" >Voir</a>');?></td>
          </tr>
          <?php
      }
    ?>
      
    </tbody>
  </table>
</div>
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center" style="display: table-cell;">
    <li class="page-item" style="list-style: none; display: table-cell; padding:10px;">
      <?php if($page == 0) { ?>
        <li class="page-item disabled">
          <a class="page-link" href="#" tabindex="-1">Précédent</a>
        </li>
      <?php } else {
        $prevPage = $page-1;
        $_GET['page'] = $prevPage;
        $build_query_http = http_build_query($_GET);
        echo('<a class="page-link" href="index.php?'.$build_query_http.'" aria-label="Précédent">')?>
          <span aria-hidden="true">Précédent</span>
        </a>

      <?php } ?>
    </li>
    <?php
    $i = max(0, $page - 5);
    for(; $i < min($page + 6, $NbPage); $i++) { 
        $j = $i+1;
        if($i == $page) {
          echo('<li class="page-item active" style="list-style: none; display: table-cell; padding:10px;"><span class="page-link" >'.$j.'</span></li>');
        } else {
          $_GET['page'] = $i;
          $build_query_http = http_build_query($_GET);
          echo('<li class="page-item" style="list-style: none;display: table-cell; padding:10px;"><a class="page-link" href="index.php?'.$build_query_http.'">'.$j.'</a></li>');
        }
    } ?>
    
    <li class="page-item" style="list-style: none; display: table-cell; padding:10px;">
      <?php if($page+1 == $NbPage) { ?>
        <li class="page-item disabled" >
          <a class="page-link" href="#" tabindex="-1">Suivant</a>
        </li>
      <?php } else {
        $nextPage = $page+1;
        $_GET['page'] = $nextPage;
        $build_query_http = http_build_query($_GET);
        echo('<a class="page-link" href="index.php?'.$build_query_http.'" aria-label="Suivant">') ?>
          <span aria-hidden="true">Suivant</span>
        </a>

      <?php } ?>
    </li>
  </ul>
</nav>

<script>
  function checkEverything() {
    var ckbox = document.getElementsByClassName('elem-check');
    for(const e of ckbox) {
      e.checked= !e.checked;
    }
  }

  function confimerRemboursement() {
    var ckbox = document.getElementsByClassName('elem-check');
    for(const e of ckbox) {
      if(e.checked) {
        // Nécessaire pour mettre tous les items selectionné en remboursé        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'index.php?uc=compt&action=confimerRemboursement');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(encodeURI('visiteur='+e.name.split(' ')[0])+'&mois='+e.name.split(' ')[1]);
      }
    }
    window.location.href = "index.php?uc=compt&action=suiviFichePaiement";
  }
  function mettreEnPaiement() {
    var ckbox = document.getElementsByClassName('elem-check');
    for(const e of ckbox) {
      if(e.checked) {
        // Nécessaire pour mettre tous les items selectionné en paiement    
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'index.php?uc=compt&action=mettreEnPaiement');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(encodeURI('visiteur='+e.name.split(' ')[0])+'&mois='+e.name.split(' ')[1]);
      }
    }
    window.location.href = "index.php?uc=compt&action=suiviFichePaiement";
  }
</script>
