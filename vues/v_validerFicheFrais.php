<h2>Valider la fiche de frais</h2>
<form>
    <input type="hidden" name="uc" value="compt">
    <input type="hidden" name="action" value="validating">
    <label for="visiteur">Visiteur : </label>
    <div>
        <select name="visiteur">
            <option value="" selected disabled hidden></option>
              <?php
                foreach($vis as $v) {
                  $nom = $v['nom'];
                  $prenom = $v['prenom'];
                  $id = $v['id'];
                  echo('<option value="'.$id.'">'.$nom.' '.$prenom.'</option>');
                }
              ?>
        </select>
    </div>
    <label for="mois">Mois : </label>
    <div>
        <input type="month" name="mois"/>
    </div>
    <div>
        <input type="submit" value="Confirmer"/>
    </div>
</form>
<?php if (isset($_GET['err'])) { ?>
    <div class="alert alert-primary" role="alert">
        Veuillez remplir tous les champs !
    </div>
<?php } ?>
<?php if (isset($_GET['notExisting'])) { ?>
    <div class="alert alert-primary" role="alert">
        Ce visiteur n'a pas de fiche de frais ce mois-ci !
    </div>
<?php } ?>
