<h2>Validation des fiches de frais</h2>
    <h4>Eléments Forfait</h4>
    <form method="post" action="index.php?uc=compt&action=updateForfait">
            <?php echo('<input type="hidden" name="visiteur" value="'.$visiteur.'">');
            echo('<input type="hidden" name="mois" value="'.$mois.'">'); ?>
            <fieldset>
                <p>
                    <label for="idFrais">Forfait Etape</label>
                    <?php echo('<input type="text" id="idFrais" name="forfaitEtape" size="10" maxlength="5" value="'.$infoFicheFraisForfait[0]['quantite'].'">');?>
                </p>
                <p>
                    <label for="idFrais">Frais Kilométrique</label>
                    <?php echo('<input type="text" id="idFrais" name="fraisKilometrique" size="10" maxlength="5" value="'.$infoFicheFraisForfait[1]['quantite'].'">');?>
                </p>
                <p>
                    <label for="idFrais">Nuitée Hôtel</label>
                    <?php echo('<input type="text" id="idFrais" name="nuitHotel" size="10" maxlength="5" value="'.$infoFicheFraisForfait[2]['quantite'].'">');?>
                </p>
                <p>
                <label for="idFrais">Repas Restaurant</label>
                    <?php echo('<input type="text" id="idFrais" name="repasResto" size="10" maxlength="5" value="'.$infoFicheFraisForfait[3]['quantite'].'">');?>
                </p>
            </fieldset>
            <div class="piedForm">
                <p>
                    <input id="ok" type="submit" value="Corriger" size="20">
                </p> 
            </div>
        </form>
    </div>
    <div>
        <h4>Eléments Hors-Forfait</h4>
        <table>
            <thead>
                <tr><td>Descriptif</td></tr>
                <tr>
                    <td><strong>Date</strong></td>
                    <td><strong>Libellé</strong></td>
                    <td><strong>Montant</strong></td>
                    <td><strong>Action</strong></td>
                </tr>
            </thead>
            <tbody>
                <?php echo('<input type="hidden" name="visiteur" value="'.$visiteur.'">');
                echo('<input type="hidden" name="mois" value="'.$mois.'">'); ?>
                <?php 
                    foreach($infoHorsForfait as $i) {
                        $libelle = $i['libelle'];
                        $date = $i['date'];
                        $idHF = $i['id'];
                        $montant = $i['montant']; ?>

                        <form method="post" action="index.php?uc=compt&action=updateHorsForfait">
                            <?php echo('<input type="hidden" name="idHF" value="'.$idHF.'">');
                            echo('<input type="hidden" name="visiteur" value="'.$visiteur.'">');
                            echo('<input type="hidden" name="mois" value="'.$mois.'">'); ?>
                            <tr>
                                <td><?php echo('<input type="text" name="date" title="Format : jj-mm-aaaa" pattern="^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$" value="'.$date.'" style="width:150px"/> '); ?></td>
                                <td><?php echo('<input type="text" name="libelle" value="'.$libelle.'" style="width:200px"/>'); ?></td>
                                <td><?php echo('<input type="number" name="montant" value="'.$montant.'" style="width:100px"/>'); ?></td>

                                <td>
                                    <button class="btn badge badge-success"><i class="icon-modify" style="color:white;"></i></button>
                                    <?php echo('<a href="index.php?uc=compt&action=validation&visiteur='.$visiteur.'&mois='.$mois.'" class="badge badge-secondary"><i class="icon-reload" style="color:white;"></i></a>');?>
                                    <?php echo('<a href="index.php?uc=compt&action=refus&idHF='.$idHF.'&visiteur='.$visiteur.'&mois='.$mois.'" class="badge badge-danger"><i class="icon-cancel" style="color:white;"></i></a>');?>
                                    <?php echo('<a href="index.php?uc=compt&action=report&idHF='.$idHF.'&visiteur='.$visiteur.'&mois='.$mois.'" class="badge badge-warning"><i class="icon-report" style="color:white;"></i></a>');?>
                                </td>
                            </tr>
                        </form>

                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<br/>
<form method="post" action="index.php?uc=compt&action=changerNbJustificatifs">
    <?php echo('<input type="hidden" name="visiteur" value="'.$visiteur.'">');
    echo('<input type="hidden" name="mois" value="'.$mois.'">'); ?>
    <div>
        <label for='nbJust'>Nombre de justificatifs : </label>
    </div>
    <div>
        <?php echo('<input type="number"  name="nbJust" class="form-control" style="width:100px" value="'.$nbJust.'"/>'); ?>
    </div>
    <div>
        <input type="submit" class="btn btn-primary" value="Confirmer"/>
    </div>
</form>
<br/>
<br/>
<div>
    <form method="post" action="index.php?uc=compt&action=validerFicheFrais">
            <?php echo('<input type="hidden" name="visiteur" value="'.$visiteur.'">');
            echo('<input type="hidden" name="mois" value="'.$mois.'">'); ?>
        <input type="submit" class="btn btn-success" value="Valider la fiche de frais" />
    </form>
</div>

<?php if(isset($_GET['val'])) { ?>
    <div class="alert alert-success" role="alert">
        Fiche de frais validée.
    </div>
<?php } ?>
