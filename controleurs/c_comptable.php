<?php
include("vues/v_sommaire.php");
$action = $_REQUEST['action'];
switch($action){
    case 'suiviFichePaiement':
        
        $itemPerPage = 10;
        $arrVisiteurs = $pdo->getVisiteursFicheFrais($_GET);
        $NbPage = ceil(count($arrVisiteurs)/$itemPerPage);

        if(!isset($_REQUEST['page'])) {
            $page = 0;
        } else {
            $page = $_REQUEST['page'];
            if ($page > $NbPage) {
                $page = $NbPage;
            }
        }
        $visiteurs = array_slice($arrVisiteurs, (($page) * $itemPerPage), $itemPerPage);
        $typeEtat = $pdo->getTypeEtat();
        $vis = $pdo->getLesVisiteursValiderFicheFrais();

        
        include('vues/v_listeVisiteur.php');
        echo("</div>");

    break;
    case 'detailFichePaiement':
        $idV = $_REQUEST['idV'];
        $date = $_REQUEST['mois'];

        $ficheFrais =$pdo->getLesInfosFicheFrais($idV, $date);
        $visiteur = $pdo->getVisiteur($idV);
        $infoEtat = $pdo->infoFraisForfait();
        $infoFicheFrais = $pdo->infoFraisForfaitByIdMois($idV, $date);
        $infoHorsForfait = $pdo->infoFraisHorsForfaitByIdMois($idV, $date);

        include('vues/v_detailFichePaiement.php');
        echo("</div>");

    break;
    case 'confimerRemboursement':
        $visiteur = $_POST['visiteur'];
        $mois = $_POST['mois'];
        $pdo->majEtatFicheFrais($visiteur, $mois, "RB");

    break;
    case 'mettreEnPaiement':
        $visiteur = $_POST['visiteur'];
        $mois = $_POST['mois'];
        $pdo->majEtatFicheFrais($visiteur, $mois, "VA");

    break;

	case 'valider':
        $vis = $pdo->getLesVisiteursValiderFicheFrais();
        include('vues/v_validerFicheFrais.php');
        echo("</div>");
    break;
    
    case 'validating':
        if($_GET['visiteur'] == '' || $_GET['mois'] == '') {
            header("Location: index.php?uc=compt&action=valider&err");
        } else {
            $mois = str_replace('-', '',$_REQUEST['mois']);
            $hasFicheFrais = $pdo->estPremierFraisMois($_GET['visiteur'], $mois);
            if ($hasFicheFrais) {
                header("Location: index.php?uc=compt&action=valider&notExisting");
            } else {
                header('Location: index.php?uc=compt&action=validation&visiteur='.$_GET['visiteur'].'&mois='.$mois);
            }
        }
    break; 
    
    case 'validation':
        $mois=$_REQUEST['mois'];
        $visiteur = $_REQUEST['visiteur'];

        $nbJust = $pdo->getNbjustificatifs($visiteur, $mois);
        $infoFicheFraisForfait = $pdo->infoFraisForfaitByIdMois($visiteur, $mois);
        $infoHorsForfait = $pdo->infoFraisHorsForfaitByIdMois($visiteur, $mois);

        include('vues/v_c_validationFicheFrais.php');
        echo("</div>");
    break;
    case 'changerNbJustificatifs':
        $nbJust = $_POST['nbJust'];
        $mois=$_POST['mois'];
        $visiteur = $_POST['visiteur'];
        $pdo->majNbJustificatifs($visiteur, $mois, $nbJust);
        header('Location: index.php?uc=compt&action=validation&visiteur='.$visiteur.'&mois='.$mois);
    break;
    case 'updateForfait':
        $mois=$_POST['mois'];
        $visiteur = $_POST['visiteur'];
        $frais = array('ETP'=>$_POST['forfaitEtape'],
                    'KM'=>$_POST['fraisKilometrique'],
                    'NUIT'=>$_POST['nuitHotel'],
                    'REP'=>$_POST['repasResto']);
        $pdo->majFraisForfait($visiteur,$mois, $frais);
        header('Location: index.php?uc=compt&action=validation&visiteur='.$visiteur.'&mois='.$mois);

    break;

    case 'updateHorsForfait':
        $idHF = $_POST['idHF'];
        $date = $_POST['date'];
        $montant = $_POST['montant'];
        $libelle = $_POST['libelle'];
        $mois=$_POST['mois'];
        $visiteur = $_POST['visiteur'];
        
        $pdo->majHorsForfait($idHF, $libelle, $date, $montant);
        header('Location: index.php?uc=compt&action=validation&visiteur='.$visiteur.'&mois='.$mois);

    break;
    case 'refus':
        $mois=$_GET['mois'];
        $visiteur = $_GET['visiteur'];
        $idHF = $_GET['idHF'];
        $pdo->refusHorsForfait($idHF, $visiteur, $mois);

        header('Location: index.php?uc=compt&action=validation&visiteur='.$visiteur.'&mois='.$mois);
    break;
    case 'report':
        $mois=$_GET['mois'];
        $visiteur = $_GET['visiteur'];
        $idHF = $_GET['idHF'];
        $pdo->reportHorsForfait($idHF);
        header('Location: index.php?uc=compt&action=validation&visiteur='.$visiteur.'&mois='.$mois);
    break;

    case 'validerFicheFrais':
        $mois=$_POST['mois'];
        $visiteur = $_POST['visiteur'];

        $pdo->majEtatFicheFrais($visiteur, $mois, "VA");
        header('Location: index.php?uc=compt&action=validation&val&visiteur='.$visiteur.'&mois='.$mois);
    break;
}
?>
